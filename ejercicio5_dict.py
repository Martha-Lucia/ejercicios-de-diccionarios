# Autora:"Martha Cango"
# Email:"martha.cango@unl.edu.ec"
# Read a string:
N = int(input())
# s = input()
# Print a value:
# print(s)
words = {}
for i in range(N):
    n = list(input().split())
    words[n[0]] = n[1:len(n)]

M = int(input())
for j in range(M):
    m = list(input().split())
    if "W" in words[m[1]] and m[0] == "write":
        print("OK")
    elif "R" in words[m[1]] and m[0] == "read":
        print("OK")
    elif "X" in words[m[1]] and m[0] == "execute":
        print("OK")
    else:
        print("Access denied")