# Autora:"Martha Cango"
# Email:"martha.cango@unl.edu.ec"
# El texto se da en una sola línea. Para cada palabra del texto, cuenta
# el número de veces que aparece antes.
# Read a string:
usuario = input().split()
# Print a value:
# print(s)
num_veces = {}
for palabra in usuario:
    if palabra not in num_veces:
        num_veces[palabra] = 0
    print(num_veces[palabra], end=' ')
    num_veces[palabra] = num_veces[palabra] + 1
