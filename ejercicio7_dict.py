# Autora:"Martha Cango"
# Email:"martha.cango@unl.edu.ec"
# Dado un número n, seguidode n líneas de texto, imprima todas las palabras encontradas
# en el texto, una por línea, con su número de apariciones en el texto. Las palabras deben
# ordenarse en orden descendente de acuerdo con su número de ocurrencias, y todas las
# palabras dentro de la misma frecuencia deben imprimirse en orden lexicográfico.
# Read a string:
n = int(input())
# Print a value:
# print(n)
text = {}
for i in range(n):
    for word in input().split():
        if word not in text:
            text[word] = 0
        text[word] = text[word] + 1

for a in sorted(set(text.values()), reverse=True):
    for word in sorted(text):
        if text[word] == a:
            print(word, a)