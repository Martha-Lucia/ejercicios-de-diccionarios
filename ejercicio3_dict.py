# Autora:"Martha Cango"
# Email:"martha.cango@unl.edu.ec"
# La primera línea contiene el número de registros. Después de eso,cada entrada
# contiene el nombre del candidato y el número de votos que obtuvieron en algún estado.
# Cuente los resultados de las elecciones:sume el número de votos para cada candidato.
# Imprimir candidatos en orden alfabético.
# Read a string:
s = int(input())
# Print a value:
# print(s)
total_votos = {}
for i in range(s):
    candidato, n_votos = input().split()
    if candidato not in total_votos:
        total_votos[candidato] = 0
    total_votos[candidato] += int(n_votos)
for candidato in sorted(total_votos):
    print(candidato, total_votos[candidato])