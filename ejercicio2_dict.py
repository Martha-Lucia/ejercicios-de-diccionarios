# Autora:"Martha Cango"
# Email:"martha.cango@unl.edu.ec"
# Te dan un diccionario que constiste en pares de palabras. Cada palabra es sinónimo de
# la otra palabra en su par.Todas las palabras del diccionario son diferentes.Después del
# diccionario hay una palabra más dada.Encuentra un sinónimo de él.
# Read a string:
word = int(input())
# Print a value:
# print(s)
pares_words = {}
for i in range(word):
    word1, word2 = input().split()
    pares_words[word1] = word2
    pares_words[word2] = word1
print(pares_words[input()])
