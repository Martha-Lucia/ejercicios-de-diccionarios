# Autora:"Martha Cango"
# Email:"martha.cango@unl.edu.ec"
# Read a string:
s = int(input())
# Print a value:
# print(s)
ingles_latino = {}
for i in range(s):
    ingles, latino = input().split("-")
    for palabra in latino.split(","):
        if palabra not in ingles_latino:
            ingles_latino[palabra] = []
        ingles_latino[palabra].append(ingles)
print(len(ingles_latino))
for palabra in sorted(ingles_latino):
    print(palabra, "-", ",".join(sorted(ingles_latino[palabra])))
