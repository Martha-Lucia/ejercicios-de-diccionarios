# Autora:"Martha Cango"
# Email:"martha.cango@unl.edu.ec"
# Dado el texto: la primera línea contiene el número de líneas,luego dadas las líneas
# de palabras.Imprima la palabra en el texto que se produce con más frecuencia.Si hay
# muchas de estas palabras, imprima la que esté menos en orden alfabético.
# Read a string:
s = int(input())
# Print a value:
# print(s)
cont_palabra = {}
for i in range(s):
    palabras = input().split()
    for palabra in palabras:
        if palabra not in cont_palabra:
            cont_palabra[palabra] = 0
        cont_palabra[palabra] += 1
palabra_repetida = max(cont_palabra.values())
for palabra in sorted(cont_palabra):
    if cont_palabra[palabra] == palabra_repetida:
        print(palabra)
        break
